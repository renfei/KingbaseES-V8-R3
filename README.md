# 人大金仓-KingbaseES-V8-R3
KingbaseES-V8 分为 R3 和 R6 版本，相互之间并不兼容，驱动包版本不对也无法连接数据库，所以请核对版本号。

由于官方已经下线了 R3 的下载页面，我就下载了R3 的安装包，并保存下来以供需要时下载。

## Docker版本

参见仓库：[https://github.com/renfei/kingbase-es-v8-r3-docker](https://github.com/renfei/kingbase-es-v8-r3-docker)

使用的人大金仓版本号：V008R003C002B0320

教程文章：[在苹果 MacOS 上基于 Docker 容器运行人大金仓（Kingbase）V8 R3 数据库的教程](https://www.renfei.net/posts/1003506)

## 仓库镜像

为方便国内用户，此处提供多个仓库镜像，国内用户可以选择：极狐

- 极狐：[https://gitlab.cn/renfei/KingbaseES-V8-R3](https://gitlab.cn/renfei/KingbaseES-V8-R3)
- GitLab：[https://gitlab.com/renfei/KingbaseES-V8-R3](https://gitlab.com/renfei/KingbaseES-V8-R3)

## 安装包
### Windows - X64

**下载链接：** [KingbaseES_V008R003C002B0340_Win64_install.iso](./Windows/KingbaseES_V008R003C002B0340_Win64_install.iso)
- **数据库版本：** V008R003C002B0340
- **文件大小：** 705，525KB
- **MD5：** D8D8F1B5DBBFC217EE1CA15F7D4E66FF
- **SHA1：** CBA23BE74FCB49331B3717E2E192018CA67F3225

### Linux - X86_64

**下载链接：** [KingbaseES_V008R003C002B0340_Lin64_install.iso](./Linux/KingbaseES_V008R003C002B0340_Lin64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 649,041KB
- **MD5：** 26BD6696FC391058251F9CDEF890C5DC
- **SHA1：** 718A49248554F7AC0BE6F2883A57566B9B3594E1

### 飞腾+Linux

**下载链接：** [KingbaseES_V008R003C002B0340_Aarch64_install.iso](./飞腾/KingbaseES_V008R003C002B0340_Aarch64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 607,330KB
- **MD5：** 7C04D6B1CFC568EE75CB6F705747041A
- **SHA1：** B4F1F5137A6AFD481AA327E3DAE826681D6050D1
- **安装包说明：**
- **兼容型号：** 1500A、2000+

### 鲲鹏+Linux

**下载链接：** [KingbaseES_V008R003C002B0340_Aarch64_install.iso](./鲲鹏/KingbaseES_V008R003C002B0340_Aarch64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 607,330KB
- **MD5：** 7C04D6B1CFC568EE75CB6F705747041A
- **SHA1：** B4F1F5137A6AFD481AA327E3DAE826681D6050D1
- **安装包说明：**
- **兼容型号：** 916、920

### 兆芯+Linux

**下载链接：** [KingbaseES_V008R003C002B0340_Lin64_install.iso](./兆芯/KingbaseES_V008R003C002B0340_Lin64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 649,041KB
- **MD5：** 26BD6696FC391058251F9CDEF890C5DC
- **SHA1：** 718A49248554F7AC0BE6F2883A57566B9B3594E1
- **安装包说明：**
- **兼容型号：** E

### 海光+Linux

**下载链接：** [KingbaseES_V008R003C002B0340_Lin64_install.iso](./海光/KingbaseES_V008R003C002B0340_Lin64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 649,041KB
- **MD5：** 26BD6696FC391058251F9CDEF890C5DC
- **SHA1：** 718A49248554F7AC0BE6F2883A57566B9B3594E1
- **安装包说明：**
- **兼容型号：** 二代

### 龙芯+Linux

**下载链接：** [KingbaseES_V008R003C002B0340_Mips64_install.iso](./龙芯/KingbaseES_V008R003C002B0340_Mips64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 637，472KB
- **MD5：** 95DE788745C20CCAF1CFD37C41460454
- **SHA1：** BA8713AC85EE79861A722EA8B4E583192FBFA465
- **安装包说明：**
- **兼容型号：** 3A3000、3B3000、3A4000、3B4000

### 开发版

#### Linux-X86

**下载链接：** [KingbaseES_V008R003C002B0340_Lin64_install.iso](./开发版/Linux/KingbaseES_V008R003C002B0340_Lin64_install.iso)

- **数据库版本：** V008R003C002B0340
- **文件大小：** 649,041KB
- **MD5：** 26BD6696FC391058251F9CDEF890C5DC
- **SHA1：** 718A49248554F7AC0BE6F2883A57566B9B3594E1

#### Windows-X86_64

- **下载链接：** [KingbaseES_V008R003C002B0340_Win64_install.iso](./开发版/Windows/KingbaseES_V008R003C002B0340_Win64_install.iso)
- **数据库版本：** V008R003C002B0340
- **文件大小：** 705，525KB
- **MD5：** D8D8F1B5DBBFC217EE1CA15F7D4E66FF
- **SHA1：** CBA23BE74FCB49331B3717E2E192018CA67F3225

## 接口驱动

由于驱动包众多，此处不再一一列举下载链接，请到 [drive](./drive/) 文件夹中下载自己所需的对应驱动包。

### QT4

QT4驱动主要运用于客户开发的Qt4项目通过QSql模块访问ES数据库

​	目前提供以下版本

​		Linux-x86:

​			kingbase-Qt-V8-4.8.6-linux-x86-64.rar

​		Linux-飞腾:

​			kingbase-Qt-V8-4.8.7-feiteng1500a.tar.gz

​		Linux-龙芯:

​			kingbase-Qt-V8-4.8.6-mips-64.tar.gz

​		Windows-32位:

​			kingbase-Qt-msvc-V8-4.8.6-windows-x86-32.rar

### QT5

QT5驱动主要运用于客户开发的Qt5项目通过QSql模块访问ES数据库

目前提供以下版本

Linux-x86:

kingbase-Qt-V8-5.6.2-linux-x86-64.rar

Linux-飞腾:

kingbase-Qt-V8-5.6.1-feiteng1500a.tar.gz

Linux-龙芯:

kingbase-Qt-V8-5.6.2-mips-64.tar.gz

Windows-32位:

kingbase-Qt-msvc-V8-5.9.0-windows-x86-64.rar

### Perl

PDI驱动是面向用户提供使用 Perl 语言访问数据库的接口，使用户可以在使用 Perl语言开发时使用KingbaseES 数据库。

​	目前提供以下版本，都是多线程版本

​		Linux-x86:

​			pdi_kdb_x86_for_perl-5.16.3.tar.gz

​			pdi_kdb_x86_for_perl-5.18.2.tar.gz

​			pdi_kdb_x86_for_perl-5.28.0.tar.gz

​		Linux-飞腾:

​			pdi_kdb_aarch64_for_perl-5.18.2.tar.gz

​			pdi_kdb_aarch64_for_perl-5.26.3.tar.gz

​			pdi_kdb_aarch64_for_perl-5.28.0.tar.gz

​		Linux-龙芯:

​            pdi_kdb_mips64_for_perl-5.16.3.tar.gz

​			pdi_kdb_mips64_for_perl-5.18.2.tar.gz		

​			pdi_kdb_mips64el_for_perl-5.26.3.tar.gz	

​			pdi_kdb_mips64el_for_perl-5.28.0.tar.gz

​		Windows-64位:

​			pdi_kdb_x64_for_perl-5.16.3_multi.tar.gz

​		Windows-32位:

​			pdi_kdb_x86_for_perl-5.16.3_multi.tar.gz

### PHP

PDO驱动是通过 PHP 提供的 ZEND ENGINE 开发的针对 PHP 访问 KingbaseES 服务器的驱动程序。

​	非线程安全的提供以下版本：

​		Linux-x86:

​			pdo_kdb_for_php-5.2.x.tar.gz

​			pdo_kdb_for_php-5.3.3.tar.gz

​			pdo_kdb_for_php-5.4.x.tar.gz

​			pdo_kdb_for_php-5.5.x.tar.gz

​			pdo_kdb_for_php-5.6.2.tar.gz

​			pdo_kdb_for_php-5.6.22.tar.gz

​			pdo_kdb_for_php-7.0.17.tar.gz

​			pdo_kdb_for_php-7.1.33.tar.gz

​			pdo_kdb_for_php-7.2.0.tar.gz

​			pdo_kdb_for_php-7.2.22.tar.gz

​			pdo_kdb_for_php-7.3.11.tar.gz

​			pdo_kdb_for_php-7.4.5.tar.gz

​			pdo_kdb_for_php-7.4.9.tar.gz

​			pdo_kdb_x64_for_php7.2.9_nts.zip

​		Linux-飞腾:

​			pdo_kdb_for_php-5.6.22_aarch64.tar.gz

​			pdo_kdb_for_php-7.0.17_aarch64.tar.gz

​			pdo_kdb_for_php-7.1.33_aarch64.tar.gz

​			pdo_kdb_for_php-7.2.0_aarch64.tar.gz

​			pdo_kdb_for_php-7.2.33_aarch64.tar.gz

​			pdo_kdb_for_php-7.3.5_aarch.tar.gz

​			pdo_kdb_for_php-7.3.11_aarch64.tar.gz

​		Linux-龙芯:

​			pdo_kdb_for_php-5.4.0.tar.gz

​			pdo_kdb_for_php-5.4.16.tar.gz

​			pdo_kdb_for_php-5.6.x_mips.tar.gz

​			pdo_kdb_mips_for_php-7.2.0.tar.gz

​			pdo_kdb_for_php-7.3.24_mips64el.tar.gz

​		Linux-申威:

​			pdo_kdb_for_php-7.2.0_sw_64.tar.gz

​	线程安全的提供以下版本：

​		Windows-64位:

​			pdo_kdb_x64_for_php7.2.0.zip

​			pdo_kdb_x64_for_php7.2.14.zip

​			pdo_kdb_x64_for_php7.2.26.zip

​		Windows-32位:

​			pdo_kdb_x86_for_php5.3.x.zip

​			pdo_kdb_x86_for_php5.6.25.zip

​			pdo_kdb_x86_for_php7.2.x.zip			

​		Linux-x86:

​			pdo_kdb_for_php-Linux-x64-5.6.22-zts.tar.gz

​			pdo_kdb_for_php-7.4.15-zts.tar.gz

​			pdo_kdb_for_php-8.0.2-zts.tar.gz

​		Linux-龙芯

​			pdo_kdb_for_php-7.4.15_aarch64_zts.tar.gz

​			pdo_kdb_for_php-8.0.2_aarch64_zts.tar.gz

​	ThinkPHP提供以下版本:

​		thinkphp5.0.24.tar.gz

### ODBC

ODBC驱动是纯C的 ODBC 驱动程序，它支持 Microsoft ODBC 3.0 标准。

​	目前提供以下版本

​		Linux-x86:

​			kingbase-odbc-linux-x86_64.rar

​		Linux-飞腾:

​			kingbase-odbc-linux-aarch64.rar

​		Linux-龙芯:

​			kingbase-odbc-linux-mips64.tar

​		Linux-申威:

​			kingbase-odbc-sw_64.tar.gz

​		Windows-64位:

​			kingbase-odbc-windows-64.rar

​		Windows-32位:

​			kingbase-odbc-windows-32.rar

### DCI

DCI驱动是KingbaseES 兼容 Oracle 的数据访问接口 Database Call Interface（简称DCI）

目前提供以下版本

Linux-x86:

​	libdcikdb.so

Linux-飞腾:

​	libdcikdb.so

Linux-龙芯:

​	libdcikdb.so

Linux-申威

  dci_sw_64.tar.gz

Windows-64位:

​	vs2008_release_dci_64\dcikdb.dll

Windows-32位:

​	vs2008_release_dci_32\dcikdb.dll

​	vs2010_release_dci_32\dcikdb.dll

### Node.js

Nodejs驱动是一些Nodejs模块的集合，用于与KingbaseES数据库交互。

目前支持以下版本

Nodejs-8

Nodejs-10

Nodejs-12

### NDP

目前提供以下版本和EF框架

​		kdbndp:

​			net40

​			net45

​			net451

​			net461

​			netcore2.0

​			netcore2.1

​			netcore3.0

​		ef6:

​			net40

​			net45

​		efcore:

​			netcore2.0

​			netcore3.0

### MyBatis-Plus

MyBatis-Plus 包是一个 Mybatis的增强工具并支持ES数据库。

​	目前提供以下版本

​			mybatis-plus-2.2.0

​			mybatis-plus-3.1.2

### JDBC

是纯 JAVA 的 JDBC 驱动程序，它支持 SUN JDBC 3.0 和 部分 4.2 API 的标准。

​	JDBC驱动目前提供以下版本

​		JDK1.6及以上:

​			kingbase8-8.2.0.jre6.jar

​		JDK1.7及以上:

​			kingbase8-8.2.0.jre7.jar

​		JDK1.8及以上:

​			kingbase8-8.2.0.jar

### Hibernate

hibernate方言包是ES为Hibernate各个版本提供的方言扩展包

​	hibernate方言包目前提供以下版本

​			hibernate-2.0.3dialect.jar[2.0,2.1）

​			hibernate-2.1dialect.jar[2.1]

​			hibernate-3.1dialect.jar[3.1,3.2）

​			hibernate-3.2.7gadialect.jar[3.2,3.3）

​			hibernate-3.3dialect.jar[3.3,3.6）

​			hibernate-3.6.10dialect.jar[3.6,4.0）

​			hibernate-4.0.1finaldialect.jar[4.0,4.3）

​			hibernate-4.3.11finaldialect.jar[4.3,5.0）

​			hibernate-4.3.2finaldialect.jar[4.3,5.0）

​			hibernate-5.0.12.Finaldialect.jar[5.0,5.2）

​			hibernate-5.2.17.Finaldialect.jar[5.2,5.4）

​			hibernate-5.4.16.Finaldialect.jar[5.4,6.0）

​	hibernate-spatial方言包目前提供以下版本

​			hibernate-spatial-kingbase-5.3.7.jar

### Golang

Gokb驱动是基于database/sql包的完全由Golang编写的kingbase驱动，它为数据库提供了轻量级的访问接口。

​	目前提供以下版本

​		Linux-x86:

​			gokb-linux-amd64-1.10.5.tar.gz

​			gokb-linux-amd64-1.11.5.tar.gz

​		Linux-飞腾:

​			gokb-linux-arm64-1.11.5.tar.gz

​		Linux-龙芯:

​			gokb-linux-mips64-1.11.5.tar.gz

### Activiti

Activiti包是Activiti工作流引擎支持ES的版本

目前提供以下版本

activiti-engine-5.10.jar 

activiti-engine-5.14.jar

activiti-engine-5.20.0.jar

activiti-engine-5.21.0.jar

activiti-engine-6.0.0.jar

### Python

ksycopg2是Python操作Kingbase数据库的驱动库，它支持完整的Python DBAPI 2.0标准。

​	目前提供以下版本

​		Linux-x86:

​			ksycopg2_linux_amd64_python2.7.tar.gz

​			ksycopg2_linux_amd64_python2.7_ucs4.tar.gz

​			ksycopg2_linux_amd64_python3.6.tar.gz

​		Linux-飞腾:

​			ksycopg2_linux_aarch64_python3.6.tar.gz

​		Windows-64位:

​			ksycopg2_windows_amd64_MSVC2017_python3.6-64bit.zip

​			ksycopg2-windows-amd64-MSVC2013-python3.7-64bit.zip

​	  sqlalchemy:

​		  sqlalchemy.tar

## License

由于License众多，此处不在一一列出下载链接，请到 [License](./License) 文件夹下载自己对应的文件。

**注意事项**

开发版License非商业版本，供应用开发商或个人开发使用。其License使用说明如下：

- 1、不限用户使用时间。
- 2、不限用户MAC地址。
- 3、最大连接数限制为10。

注意开发版限制最大连接数为10。

## 配套文档

### 版本兼容性列表

[KingbaseES V008R003C002B0270版本兼容性列表v1.0.xlsx](./Document/KingbaseES V008R003C002B0270版本兼容性列表v1.0.xlsx)

## 版本差异说明

| 科目                 | 试用版 | 标准版   | 专业版   | 企业版   | 备注                                                         |
| -------------------- | ------ | -------- | -------- | -------- | ------------------------------------------------------------ |
| 授权                 |        |          |          |          |                                                              |
| 类型                 | 免费   | 商业授权 | 商业授权 | 商业授权 | 试用版：用于教学，测试等非商业、非生产用途；提供临时授权   标准版：限25用户数 和 不限用户数 两个报价模式   专业版：按CPU 报价模式   企业版：按CPU 报价模式 |
| 功能                 |        |          |          |          |                                                              |
| MAC地址              | √      | √        | √        | √        |                                                              |
| 最大连接数           | √      | √        | √        | √        |                                                              |
| 数据同步             | √      | √        | √        | √        |                                                              |
| 对象管理             | √      | √        | √        | √        | 对数据库中表、视图、存储过程、函数等对象进行管理             |
| 应用接口             | √      | √        | √        | √        | 支持ODBC、JDBC等开发接口，支持hibernate、EF、mybatis、activity、.Net  core等开发框架 |
| X86-Windows平台      | √      | √        | -        | -        | Windows平台支持标准版，只是试用申请。不支持专业版和企业版。   建议用户生产环境换成非windows操作系统平台 |
| X86-Linux平台        | √      | √        | -        | √        | 专业版功能项是参照入名录要求，信创项目需求；因此非全国产硬件平台需要额外付费   X86-linux平台：即非国产CPU平台 |
| 国产硬件平台         | √      | -        | √        | √        | 支持国产CPU通用服务器或保密机                                |
| 工具                 |        |          |          |          |                                                              |
| 命令行工具           | √      | √        | √        | √        | 命令行管理工具                                               |
| 图形化工具           | √      | √        | √        | √        | 图形化管理工具                                               |
| 高可用               |        |          |          |          |                                                              |
| 物理同步             | √      | -        | √        | √        | 数据库基础功能，是双机热备集群的必需特性                     |
| 读写分离模块         | √      | -        | 选配     | 选配     | 专业版、企业版选购：需要额外收费；    配合读写分离集群软件实现集群高用；是数据库的功能组件，负责数据库节点状态、健康检查、主备切换等响应。 |
| 滚动升级             | √      | -        | -        | √        | 传统升级是需要通过备份、还原等操作，滚动升级实现智能化升级操作，通过一键操作实现升级功能 |
| 增量备份恢复         | √      | -        | √        | √        | 物理增量备份恢复，减少备份集空间占用，提高备份性能，减少对系统运行的IO影响。 |
| 恢复到指定时间点     | √      | -        | -        | √        | 基于备份文件和归档文件恢复到指定时间点，按需进行数据还原     |
| 逻辑备份还原         | √      | √        | √        | √        | 备份数据库对象定义和数据                                     |
| 集群对网络故障的容错 | √      | -        | √        | √        | 提高数据健壮性，建立网终故障容错处理机制                     |
| 性能                 |        |          |          |          |                                                              |
| 快速加载             | √      | -        | -        | √        | 实现数据批量快速导入                                         |
| 日志压缩             | √      | -        | -        | √        | 减少日志占用空间以及提高日志传输效率                         |
| 全文检索             | √      | -        | -        | √        | 支持数据模糊查询、提高数据检索效率                           |
| 安全                 |        |          |          |          |                                                              |
| 保密通讯协议         | √      | -        | √        | √        | 针对专用机环境相关安全需求，定制开发的保密通讯协议           |
| 审计                 | √      | -        | √        | √        | 记录数据库的相关操作，便于入侵检测和事后追查                 |
| 三权分立             | √      | -        | √        | √        | 数据库分为系统管理员、审计员、安全员三个角色用户，进行权限划分提高安全性 |
| 透明加密             | √      | -        | √        | 选配     | 企业版选购：需要额外收费    数据库存储文件加密，对上层应用透明，防止数据文件盗取后数据安全。因此，在对应用系统安全性需求较高的使用场景进行推荐，或者做为保障数据库安全的卖点进行推荐 |
| 强制访问控制         | √      | -        | √        | 选配     | 企业版可选购：需要额外收费    第三级安全数据库的核心，对用户和数据授予不同的标记，通过比较用户和数据的标记来进行访问控制。因此，在对应用系统安全性需求较高的使用场景进行推荐，或者作为保障数据库安全的卖点进行推荐。提高数据库系统的访问控制能力到强制访问控制级别，给敏感数据提供特殊的保护，提高数据库安全性。 |
| 密码复杂度           | √      | -        | √        | √        | 提高数据库密码复杂度要求，避免用户密码过于设置简单           |
| 用户锁定             | √      | -        | √        | √        | 基于安全考虑，针对尝试失败后锁住用户等特性                   |
| 可管理性             |        |          |          |          |                                                              |
| 运行状态分析         | √      | -        | √        | √        | 数据库运行状态分析                                           |
| 健康检查             | √      | -        | -        | √        | 在线检查数据库文件完整性、数据文件、日志文件等完整性         |
| 集群配置工具         | √      | -        | √        | √        | 图形配置部署集群工具                                         |
| 大数据管理           |        |          |          |          |                                                              |
| 并行查询             | √      | -        | -        | √        | 多进程并行执行查询操作，提高执行效率                         |
| 并行备份还原         | √      | -        | -        | √        | 多进程进行备份还原操作，提高执行效率                         |
| 数据集成             |        |          |          |          |                                                              |
| 日志解析             | √      | -        | -        | 选配     | 企业版选购：需要额外付费   针对日志文件进行特定的解析输入，方便数据同步软件进行数据同步，是基于日志进行数据同步的基础功能。因此，在有异构数据同步，或者ES  数据库之间进行逻辑同步的场景中可以推荐 |
| 异构数据源           | √      | -        | -        | 选配     | 支持访问异构数据库中的表数据，进行跨库访问                   |